---
layout: post
title: Welcome to the MediaPlay blog!
---

This will be the place where MediaPlay members can write their own article in here!

Read the [README.md file in the repository](https://github.com/MediaPlay-Discord/blog#readme) for more information.



— Linerly