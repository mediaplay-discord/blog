# The MediaPlay Blog

Welcome to the MediaPlay blog repository! MediaPlay members can write and submit their own article here.

# Submitting Your Own Artcle

Your article can be pretty anything that is related to MediaPlay, just make sure to use your common sense.

## How to Submit Your Own Article

You can either submit your own article to Linerly (#development-chat), or do a pull request in this repository if you have a GitHub account.
